<?php
/**
 * Template for rendering a `checkboxes` filter in Explore page.
 *
 * @since 1.0
 */
if ( ! defined('ABSPATH') ) {
    exit;
}

// must be a valid listing field
if ( ! ( $field = $type->get_field( $filter->get_prop('show_field') ) ) ) {
    return;
}

$choices = $filter->get_choices();
$selected = $filter->get_request_value();
$fieldkey = sprintf( 'types["%s"].filters["%s"]', $type->get_slug(), $filter->get_prop('show_field') );
?>

<div class="form-group form-group-tags explore-filter checkboxes-filter" data-key="<?php echo esc_attr( $filter->get_prop('show_field') ) ?>">
	<label><?php echo esc_html( $filter->get_label() ) ?></label>
	<ul class="tags-nav">
		<?php foreach ( (array) $choices as $key => $choice ):
            $choice_id = $filter->get_unique_id().'-'.$key ?>
			<li>
				<div class="md-checkbox">
					<input
                        id="<?php echo esc_attr( $choice_id ) ?>"
                        type="<?php echo $filter->get_prop('multiselect') ? 'checkbox' : 'radio' ?>"
                        value="<?php echo esc_attr( $choice['value'] ) ?>"
                        v-model="<?php echo esc_attr( $fieldkey ) ?>"
                        @change="getListings( 'checkboxes' )"
                    >
					<label for="<?php echo esc_attr( $choice_id ) ?>" class=""><?php echo esc_attr( $choice['label'] ) ?></label>
				</div>
			</li>
		<?php endforeach ?>
	</ul>
</div>
