<?php
/**
 * Template for rendering a `range` filter in Explore page.
 *
 * @since 1.0
 */
if ( ! defined('ABSPATH') ) {
    exit;
}

$fieldkey = sprintf( 'types[\'%s\'].filters[\'%s\']', $type->get_slug(), $filter['show_field'] );
$value = $filter->get_request_value();
$range_type = $filter->get_prop('option_type') === 'simple' ? 'single' : 'range';
?>

<div class="form-group radius radius1 range-slider explore-filter range-filter" data-type="<?php echo $range_type ?>">
	<label><?php echo esc_html( $filter->get_label() ) ?></label>
    <div
        class="mylisting-range-slider"
        data-name="<?php echo esc_attr( $filter->get_prop('url_key') ) ?>"
        data-type="<?php echo $range_type ?>"
        data-min="<?php echo esc_attr( $filter->get_range_min() ) ?>"
        data-max="<?php echo esc_attr( $filter->get_range_max() ) ?>"
        data-prefix="<?php echo esc_attr( $filter->get_prop('prefix') ) ?>"
        data-suffix="<?php echo esc_attr( $filter->get_prop('suffix') ) ?>"
        data-step="<?php echo esc_attr( $filter->get_prop('step') ) ?>"
        data-start="<?php echo ! empty( $value['start'] ) ? esc_attr( $value['start'] ) : false ?>"
        data-end="<?php echo ! empty( $value['end'] ) ? esc_attr( $value['end'] ) : false ?>"
        data-localize="<?php echo $filter->get_prop('format_value') ? 'yes' : 'no' ?>"
        @rangeslider:change="<?php echo esc_attr( $fieldkey ) ?> = $event.detail.value; getListings( 'range-filter' );"
    ></div>
</div>
